<?php

namespace Drupal\Tests\linkchecker_summary_mail\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the link checker summary mail config form.
 *
 * @group linkchecker_summary_mail
 */
class ConfigFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['linkchecker_summary_mail'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * User with the administer linkchecker permission.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * A regular user.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $webUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->webUser = $this->createUser();
    $this->adminUser = $this->createUser(['administer linkchecker']);
  }

  /**
   * Tests the link checker summary mail config form.
   */
  public function testLinkcheckerSummaryMailConfigForm() {
    // Check default config.
    $config = $this->config('linkchecker_summary_mail.settings');
    $this->assertEquals('daily', $config->get('interval'));
    $this->assertEquals(TRUE, $config->get('enable_global'));
    $this->assertEquals($this->config('system.site')->get('mail'), $config->get('mail_address'));
    $this->assertEquals(FALSE, $config->get('notify_author'));
    $this->assertEquals(FALSE, $config->get('notify_latest_editor'));
    $this->assertEquals(FALSE, $config->get('summarize_all'));

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('linkchecker_summary_mail.settings'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as a regular user.
    $this->drupalLogin($this->webUser);

    // Unauthorized user should not have access.
    $this->drupalGet(Url::fromRoute('linkchecker_summary_mail.settings'));
    $this->assertSession()->statusCodeEquals(403);

    // Login as an admin user.
    $this->drupalLogin($this->adminUser);

    // Update link checker summary mail config.
    $this->drupalGet(Url::fromRoute('linkchecker_summary_mail.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $edit = [
      'interval' => 'weekly',
      'enable_global' => FALSE,
      'mail_address' => 'info@example.com',
      'notify_author' => TRUE,
      'notify_latest_editor' => TRUE,
      'summarize_all' => TRUE,
    ];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Check if config is updated.
    $config = $this->config('linkchecker_summary_mail.settings');
    $this->assertEquals('weekly', $config->get('interval'));
    $this->assertEquals(FALSE, $config->get('enable_global'));
    $this->assertEquals('info@example.com', $config->get('mail_address'));
    $this->assertEquals(TRUE, $config->get('notify_author'));
    $this->assertEquals(TRUE, $config->get('notify_latest_editor'));
    $this->assertEquals(TRUE, $config->get('summarize_all'));

    // Check if default values are correctly updated.
    $this->drupalGet(Url::fromRoute('linkchecker_summary_mail.settings'));
    $this->assertSession()->fieldValueEquals('interval', 'weekly');
    $this->assertSession()->fieldValueEquals('enable_global', FALSE);
    $this->assertSession()->fieldValueEquals('mail_address', 'info@example.com');
    $this->assertSession()->fieldValueEquals('notify_author', TRUE);
    $this->assertSession()->fieldValueEquals('notify_latest_editor', TRUE);
    $this->assertSession()->fieldValueEquals('summarize_all', TRUE);
  }

}
