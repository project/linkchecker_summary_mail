<?php

namespace Drupal\Tests\linkchecker_summary_mail\Kernel;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Test\AssertMailTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\linkchecker\Entity\LinkCheckerLink;
use Drupal\linkchecker_summary_mail\LinkcheckerSummaryMailInterval;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests the summary mail with all possible configuration options.
 *
 * @group linkchecker_summary_mail
 */
class LinkcheckerSummaryMailTest extends KernelTestBase {

  use AssertMailTrait;
  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'dynamic_entity_reference',
    'field',
    'filter',
    'linkchecker',
    'linkchecker_summary_mail',
    'node',
    'user',
    'system',
    'text',
  ];

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The linkchecker summary mail builder.
   *
   * @var \Drupal\linkchecker_summary_mail\LinkcheckerSummaryMailSummaryBuilder
   */
  protected $summaryMailBuilder;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The author of the created nodes.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $user;

  /**
   * The node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installSchema('system', ['sequences']);
    $this->installSchema('linkchecker', ['linkchecker_index']);
    $this->installConfig([
      'linkchecker',
      'linkchecker_summary_mail',
      'node',
      'filter',
    ]);
    $this->installEntitySchema('linkcheckerlink');
    $this->installEntitySchema('node');
    $this->installEntitySchema('user');

    $this->summaryMailBuilder = $this->container->get('linkchecker_summary_mail.summary_builder');
    $this->state = $this->container->get('state');
    $this->time = $this->container->get('datetime.time');

    $this->createContentType([
      'type' => 'page',
      'name' => 'Page',
    ]);

    $this->user = $this->createUser();
    $this->node = $this->createNode(['uid' => $this->user]);
  }

  /**
   * Test the linkchecker_summary_builder.settings interval option.
   */
  public function testIntervalOption() {
    $this->config('linkchecker_summary_mail.settings')->set('interval', 'daily')->save();
    $this->createDummyLink();

    // When no last checked date isset, make sure the mail is sent.
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMail('id', 'linkchecker_summary_mail_summary');

    // Reset the collected mails.
    $this->state->set('system.test_mail_collector', []);
    // Mails should not be sent in the same day.
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertEmpty($mails);

    // Send a mail after 1 day.
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', $this->time->getRequestTime() - (60 * 60 * 24) - 10);
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMail('id', 'linkchecker_summary_mail_summary');

    // Check the weekly interval time.
    $this->state->set('system.test_mail_collector', []);
    $this->config('linkchecker_summary_mail.settings')
      ->set('interval', 'weekly')
      ->save();

    // Mail should not send after 1 day.
    $this->state->set('linkchecker_summary_mail.last_checked', $this->time->getRequestTime() - (60 * 60 * 24) - 10);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertEmpty($mails);

    // Mail should send after 7 days.
    $this->state->set('linkchecker_summary_mail.last_checked', $this->time->getRequestTime() - (60 * 60 * 24 * 7) - 10);
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMail('id', 'linkchecker_summary_mail_summary');
  }

  /**
   * Test the linkchecker_summary_builder.settings enable_global option.
   */
  public function testEnableGlobalOption() {
    $this->config('linkchecker_summary_mail.settings')->set('enable_global', TRUE)->save();
    $this->config('linkchecker_summary_mail.settings')->set('mail_address', 'info@example.org')->save();
    $this->createDummyLink();

    // Mail should be sent when the enable_global options is enabled.
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $mail = current($mails);
    $this->assertEquals('info@example.org', $mail['to']);
    $this->assertEquals('linkchecker_summary_mail_summary', $mail['id']);

    // No mail should be sent when the enable_global option is disabled.
    $this->config('linkchecker_summary_mail.settings')->set('enable_global', FALSE)->save();
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertEmpty($mails);
  }

  /**
   * Test the linkchecker_summary_builder.settings notify_author option.
   */
  public function testNotifyAuthorOption() {
    $this->createDummyLink();
    $this->config('linkchecker_summary_mail.settings')->set('enable_global', FALSE)->save();
    $this->config('linkchecker_summary_mail.settings')->set('notify_author', TRUE)->save();

    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);

    foreach ($mails as $mail) {
      // Check if the mail to equals the author of the node.
      $this->assertEquals($this->node->getOwner()->getEmail(), $mail['to']);
    }
  }

  /**
   * Test the linkchecker_summary_builder.settings notify_latest_editor option.
   */
  public function testNotifyEditorOption() {
    $this->createDummyLink();
    $this->config('linkchecker_summary_mail.settings')->set('enable_global', FALSE)->save();
    $this->config('linkchecker_summary_mail.settings')->set('notify_latest_editor', TRUE)->save();

    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);

    foreach ($mails as $mail) {
      // Check if the mail to equals the latest editor of the node.
      $this->assertEquals($this->node->getRevisionUser()->getEmail(), $mail['to']);
    }
  }

  /**
   * Test the linkchecker_summary_builder.settings summarize_all option.
   */
  public function testSummarizeAllOption() {
    $this->config('linkchecker_summary_mail.settings')->set('summarize_all', TRUE)->save();
    $this->createDummyLink(NULL, 404, $this->time->getRequestTime() - ((60 * 60 * 24) * 2));

    // Mail should be sent even when the last time the link was checked was
    // after the last time the cron ran.
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMail('id', 'linkchecker_summary_mail_summary');

    // Mail should be sent even when the last time the link was checked was
    // after the last time the cron ran.
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', $this->time->getRequestTime() - (60 * 60 * 24));
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMail('id', 'linkchecker_summary_mail_summary');

    // Mail should not be sent since there were no matching links since the
    // last time the cron ran.
    $this->config('linkchecker_summary_mail.settings')->set('summarize_all', FALSE)->save();
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', $this->time->getRequestTime() - (60 * 60 * 24));
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertEmpty($mails);
  }

  /**
   * Test the mail subject.
   */
  public function testMailSubject() {
    $this->config('linkchecker_summary_mail.settings')->set('interval', 'daily')->save();
    $this->createDummyLink();

    // Test the mail subject with daily interval.
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $subject = new FormattableMarkup('Broken links found in the last @period on @site', [
      '@period' => LinkcheckerSummaryMailInterval::periodToString(LinkcheckerSummaryMailInterval::DAILY),
      '@site' => $this->config('system.site')->get('name'),
    ]);
    $this->assertEquals($subject, $mails[0]['subject']);

    // Test the mail subject with weekly interval.
    $this->state->set('system.test_mail_collector', []);
    $this->config('linkchecker_summary_mail.settings')->set('interval', 'weekly')->save();
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $subject = new FormattableMarkup('Broken links found in the last @period on @site', [
      '@period' => LinkcheckerSummaryMailInterval::periodToString(LinkcheckerSummaryMailInterval::WEEKLY),
      '@site' => $this->config('system.site')->get('name'),
    ]);
    $this->assertEquals($subject, $mails[0]['subject']);
  }

  /**
   * Test the mail body.
   */
  public function testMailBody() {
    $this->config('linkchecker_summary_mail.settings')->set('summarize_all', TRUE)->save();
    $this->config('linkchecker_summary_mail.settings')->set('enable_global', TRUE)->save();

    // Mail should not be send, when there are no links available.
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertEmpty($mails);

    $this->createDummyLink('http://example.org/link1', 404);

    // Make sure a 404 link is added to the mail.
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $this->assertMailString('body', 'http://example.org/link1', 1);

    $this->createDummyLink('http://example.org/link2', 200);

    // Check if the 404 link is still available, but to 200 link not.
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertMailString('body', 'http://example.org/link1', 1);
    $this->assertStringNotContainsString('http://example.org/link2', $mails[0]['body']);

    $this->createDummyLink('http://example.org/link3', 404);

    // Check if the mail works with multiple 404 links.
    $this->state->set('system.test_mail_collector', []);
    $this->state->set('linkchecker_summary_mail.last_checked', FALSE);
    $this->summaryMailBuilder->runCronCheck();
    $mails = $this->getMails(['id' => 'linkchecker_summary_mail_summary']);
    $this->assertMailString('body', 'http://example.org/link1', 1);
    $this->assertMailString('body', 'http://example.org/link3', 1);
    $this->assertStringNotContainsString('http://example.org/link2', $mails[0]['body']);
  }

  /**
   * Helper function for link creation.
   *
   * @param string $url
   *   The URL that needs to be checked.
   * @param int $status
   *   The HTTP status code.
   * @param int|null $last_check
   *   The last time the link was checked.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createDummyLink($url = NULL, $status = 404, $last_check = NULL) {
    LinkCheckerLink::create([
      'url' => $url ?? 'http://example.org',
      'code' => $status,
      'fail_count' => 2,
      'entity_id' => [
        'target_id' => $this->node->id(),
        'target_type' => $this->node->getEntityTypeId(),
      ],
      'entity_field' => 'dummy_field',
      'entity_langcode' => 'en',
      'last_check' => $last_check ?? $this->time->getRequestTime(),
    ])->save();
  }

}
