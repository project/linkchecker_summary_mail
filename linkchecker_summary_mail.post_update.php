<?php

/**
 * @file
 * Post update functions for Link checker summary mail.
 */

/**
 * Set a default value for notify_latest_editor.
 */
function linkchecker_summary_mail_post_update_default_value_notify_latest_editor() {
  \Drupal::configFactory()->getEditable('linkchecker_summary_mail.settings')
    ->set('notify_latest_editor', FALSE)
    ->save();
}
