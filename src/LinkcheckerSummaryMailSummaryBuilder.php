<?php

namespace Drupal\linkchecker_summary_mail;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * The class that builds and sends the summary mail.
 */
class LinkcheckerSummaryMailSummaryBuilder {

  /**
   * The linkchecker_summary_mail.settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The linkcheckerlink storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $linkcheckerlinkStorage;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Day in seconds.
   */
  protected const DAY = (60 * 60 * 24);

  /**
   * Key used to get the last time checked from the state.
   */
  protected const LAST_CHECKED_STATE_KEY = 'linkchecker_summary_mail.last_checked';

  /**
   * LinkcheckerSummaryMailSummaryBuilder constructor.
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entityTypeManager, LanguageManagerInterface $languageManager, LoggerChannelInterface $logger, MailManagerInterface $mailManager, StateInterface $state, TimeInterface $time) {
    $this->config = $configFactory->get('linkchecker_summary_mail.settings');
    $this->languageManager = $languageManager;
    $this->linkcheckerlinkStorage = $entityTypeManager->getStorage('linkcheckerlink');
    $this->logger = $logger;
    $this->mailManager = $mailManager;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Check if the summary mail should be sent.
   *
   * Called in hook_cron().
   *
   * @see linkchecker_summary_mail_cron()
   */
  public function runCronCheck(): void {
    $last_checked = $this->state->get(self::LAST_CHECKED_STATE_KEY, FALSE);

    $time_ago = NULL;
    switch ($this->config->get('interval')) {
      case LinkcheckerSummaryMailInterval::DAILY:
        $time_ago = $this->time->getRequestTime() - self::DAY;
        break;

      case LinkcheckerSummaryMailInterval::WEEKLY:
        $time_ago = $this->time->getRequestTime() - (self::DAY * 7);
        break;
    }

    if ($last_checked <= $time_ago) {
      if ($this->config->get('summarize_all')) {
        $last_checked = FALSE;
      }
      $this->buildSummaryMail($this->config->get('interval'), $last_checked);
      $this->state->set(self::LAST_CHECKED_STATE_KEY, $this->time->getRequestTime());
    }
  }

  /**
   * Builds and sends the actual summary mail.
   *
   * @param string $interval
   *   Interval for sending the mail.
   * @param int $last_checked
   *   Timestamp the last time the mail was sent.
   */
  protected function buildSummaryMail(string $interval, $last_checked): void {
    $this->logger->info('Checking for a summary for link checker with period @period', ['@period' => $interval]);

    $query = $this->linkcheckerlinkStorage->getQuery();
    $query->condition('fail_count', 0, '>')
      ->condition('status', 1)
      ->condition('code', 200, '<>');

    if ($last_checked !== FALSE) {
      $query->condition('last_check', $last_checked, '>');
    }

    $query->accessCheck();
    $links = $query->execute();

    $this->logger->info('Found a total of @num links to send a summary about.', ['@num' => count($links)]);

    if (empty($links)) {
      return;
    }

    $links = $this->linkcheckerlinkStorage->loadMultiple($links);

    $params = [
      'period' => $interval,
      'links' => $links,
    ];

    if ($this->config->get('enable_global') === TRUE) {
      $this->mailManager->mail(
        'linkchecker_summary_mail',
        'summary',
        $this->config->get('mail_address'),
        $this->languageManager->getDefaultLanguage()->getId(),
        $params
      );
      $this->logger->info('Sent a link checker summary mail to @mail. This is the global mail.', ['@mail' => $this->config->get('mail_address')]);
    }

    if ($this->config->get('notify_author') === TRUE) {
      /** @var \Drupal\linkchecker\LinkCheckerLinkInterface $link */
      foreach ($links as $link) {
        $params = [
          'period' => $interval,
          'links' => [$link],
        ];
        $this->mailManager->mail(
          'linkchecker_summary_mail',
          'summary',
          $link->getParentEntity()->getOwner()->getEmail(),
          $this->languageManager->getDefaultLanguage()->getId(),
          $params
        );
      }
    }

    if ($this->config->get('notify_latest_editor') === TRUE) {
      /** @var \Drupal\linkchecker\LinkCheckerLinkInterface $link */
      foreach ($links as $link) {
        $params = [
          'period' => $interval,
          'links' => [$link],
        ];
        $this->mailManager->mail(
          'linkchecker_summary_mail',
          'summary',
          $link->getParentEntity()->getRevisionUser()->getEmail(),
          $this->languageManager->getDefaultLanguage()->getId(),
          $params
        );
      }
    }
  }

}
