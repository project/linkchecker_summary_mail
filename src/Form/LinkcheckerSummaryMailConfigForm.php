<?php

namespace Drupal\linkchecker_summary_mail\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\linkchecker_summary_mail\LinkcheckerSummaryMailInterval;

/**
 * The linkchecker summary mail configuration form.
 */
class LinkcheckerSummaryMailConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'linkchecker_summary_mail_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['linkchecker_summary_mail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('linkchecker_summary_mail.settings');

    $form['interval'] = [
      '#title' => $this->t('Interval for mail'),
      '#description' => $this->t('Adjust the interval for the mail'),
      '#type' => 'select',
      '#options' => [
        LinkcheckerSummaryMailInterval::DAILY => $this->t('Daily'),
        LinkcheckerSummaryMailInterval::WEEKLY => $this->t('Weekly'),
      ],
      '#default_value' => $config->get('interval'),
    ];
    $form['enable_global'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable global address summary'),
      '#description' => $this->t('Check this if you want to send notifications to one specific email address'),
      '#default_value' => $config->get('enable_global'),
    ];
    $form['mail_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email adress for global summary'),
      '#default_value' => $config->get('mail_address'),
      '#states' => [
        'visible' => [
          ':input[name="enable_global"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['notify_author'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify authors about broken links'),
      '#description' => $this->t('Check this to send a notification to the author of the entity about newly found broken links.'),
      '#default_value' => $config->get('notify_author'),
    ];
    $form['notify_latest_editor'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Notify the latest editor of a node about broken links'),
      '#description' => $this->t('Check this to send a notification to the latest editor of the entity about newly found broken links.'),
      '#default_value' => $config->get('notify_latest_editor'),
    ];
    $form['summarize_all'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Summarize all broken links, always'),
      '#description' => $this->t('Check this box to always send the entire list of broken links, in the summary mails'),
      '#default_value' => $config->get('summarize_all'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('linkchecker_summary_mail.settings')
      ->set('interval', $form_state->getValue('interval'))
      ->set('enable_global', $form_state->getValue('enable_global'))
      ->set('mail_address', $form_state->getValue('mail_address'))
      ->set('notify_author', $form_state->getValue('notify_author'))
      ->set('notify_latest_editor', $form_state->getValue('notify_latest_editor'))
      ->set('summarize_all', $form_state->getValue('summarize_all'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
