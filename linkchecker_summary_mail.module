<?php

/**
 * @file
 * Sends link checker summary mails in certain intervals.
 */

use Drupal\Component\Render\FormattableMarkup;
use Drupal\linkchecker_summary_mail\LinkcheckerSummaryMailInterval;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function linkchecker_summary_mail_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the linkchecker_summary_mail module.
    case 'help.page.linkchecker_summary_mail':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module adds options to send periodic emails about
      what links are broken on your site.
      It uses the awesome Link checker module to do the heavy lifting, and only
      sends reports based on this.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_cron().
 */
function linkchecker_summary_mail_cron() {
  /** @var \Drupal\linkchecker_summary_mail\LinkcheckerSummaryMailSummaryBuilder $builder */
  $builder = \Drupal::service('linkchecker_summary_mail.summary_builder');
  $builder->runCronCheck();
}

/**
 * Implements hook_mail().
 */
function linkchecker_summary_mail_mail($key, &$message, $params) {
  if ($key === 'summary') {
    $message['subject'] = t('Broken links found in the last @period on @site', [
      '@site' => \Drupal::config('system.site')->get('name'),
      '@period' => LinkcheckerSummaryMailInterval::periodToString($params['period']),
    ]);
    /** @var \Drupal\linkchecker\LinkCheckerLinkInterface $link */
    foreach ($params['links'] as $link) {
      $parent_entity = $link->getParentEntity();
      $message['body'][] = new FormattableMarkup('<p>@title</p>', [
        '@title' => t('The link <a href="@link">@link</a> (found on <a href="@url">@url</a>) gives status code @code. This link has been checked a total of @num.', [
          '@link' => $link->getUrl(),
          '@url' => $parent_entity->hasLinkTemplate('canonical') ? $parent_entity->toUrl()->setAbsolute()->toString() : '',
          '@code' => $link->getStatusCode(),
          '@num' => \Drupal::translation()->formatPlural($link->getFailCount(), '@count time', '@count times'),
        ]),
      ]);
    }
  }
}
